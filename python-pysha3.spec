%global pypi_name pysha3

Name:           python-%{pypi_name}
Version:        1.0.2
Release:        1%{?dist}
Summary:        SHA-3 (Keccak) hash implementation for Python

License:        MIT
URL:            https://pypi.python.org/pypi/%{pypi_name}
# XXX copr doesn't support pypi_source
#Source0:        %pypi_source
Source0:        https://files.pythonhosted.org/packages/source/p/%{pypi_name}/%{pypi_name}-%{version}.tar.gz

BuildRequires:  python3-devel
BuildRequires:	gcc

%description
SHA-3 (Keccak) hash implementation for Python.


%package -n python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
%{?python_enable_dependency_generator}

%description -n python3-%{pypi_name}
SHA-3 (Keccak) hash implementation for Python.

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py3_build

%install
%py3_install

%check
%{__python3} setup.py test

# Note that there is no %%files section for the unversioned python module
%files -n python3-%{pypi_name}
%license LICENSE
%doc README.txt
%{python3_sitearch}/__pycache__/sha3*.py*
%{python3_sitearch}/sha3.py*
%{python3_sitearch}/_%{pypi_name}*.so
%{python3_sitearch}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Wed Oct 10 2018 Timothy Redaelli <tredaelli@redhat.com> - 1.0.2-1
- Initial packaging

